#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import importlib
import os
from os import path
import pkg_resources
import requests
import socket
import sys
import tempfile
import time
from typing import Any, Callable, Dict

from cloudinit.distros import rhel
from cloudinit.net import netplan
from cloudinit.net import sysconfig
from cloudinit import stages
from cloudinit import subp
from cloudinit import templater
from cloudinit import util

from . import common


USER_CONF = '/etc/kanod-configure/configuration.yaml'
SYSTEM_CONF = '/etc/kanod-configure/system.yaml'
ROOT = '/'
DEFAULT_NO_PROXY = (
    'localhost,127.0.0.1,10.96.0.0/16,192.168.0.0/16,127.0.0.1,localhost,'
    '.svc,.local,argocd-repo-server')

configuration_table: Dict[str, Callable[[stages.Init, Any, Any], None]] = {}


def register(name: str, callback: Callable[[stages.Init, Any, Any], None]):
    configuration_table[name] = callback


def render_template(name, target_path, vars, resource_package=__name__):
    '''Render a template located in template folder

    :param name: name of the template in the template folder
    :param path: path on the target file system where the template must
        be expanded
    :param vars: context dictionary to customize the template
    :param resource_package: name of resource package on which to base the
       lookup for the template.
    '''
    template_path = '/'.join(['templates', name])
    content = pkg_resources.resource_string(resource_package, template_path)
    templater.render_string_to_file(
        content.decode('utf-8'),
        path.join(ROOT, target_path),
        vars)


def propagate_var(conf, conf_var, system_var):
    '''Tale a variable from a conf file and makes it an environment variable

    :param conf: the configuration the variable belongs to
    :param conf_var: the conf variable name
    :param env_var: the env variable name
    '''
    val = conf.get(conf_var, None)
    if val is not None:
        os.environ[system_var] = val


def complete_no_proxy(sys, no_proxy):
    '''Complete no_proxy with kanod values

    Some ip and domains MUST NOT go through the proxy (mainly internal pod)
    '''
    elements = no_proxy.split(',')
    sys_proxy = sys.get('no_proxy', DEFAULT_NO_PROXY)
    added_proxy = [elt for elt in sys_proxy.split(',') if elt not in elements]
    return ','.join(elements + added_proxy)


def setup_certificates(conf):
    '''Update the certificates of the system'''
    if path.exists('/usr/local/share/ca-certificates'):
        target = path.join('/usr/local/share/ca-certificates', 'kanod')
        common.setup_certificates(conf, target, suffix='.crt')
        subp.subp(['update-ca-certificates'])
    elif path.exists('/etc/pki/ca-trust/source/anchors/'):
        target = '/etc/pki/ca-trust/source/anchors/'
        common.setup_certificates(conf, target, suffix='.crt')
        subp.subp(['update-ca-trust'])
    else:
        print('Cannot handle certificates on this system.')


def setup_proxy(sys, conf):
    '''Configure the proxy variables'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        no_proxy = proxy_vars.get('no_proxy', None)
        if no_proxy is not None:
            proxy_vars['no_proxy'] = complete_no_proxy(sys, no_proxy)
        print('Configure proxy (base)')
        render_template('environment.tmpl', 'etc/environment', proxy_vars)
        propagate_var(proxy_vars, 'http', 'http_proxy')
        propagate_var(proxy_vars, 'https', 'https_proxy')
        propagate_var(proxy_vars, 'no_proxy', 'no_proxy')


def wait_for_vault(vault_url, verify):
    while True:
        try:
            req = requests.get(f'{vault_url}/v1/sys/health', verify=verify)
            if req.status_code == 200:
                if not req.json().get('sealed', True):
                    break
                else:
                    print('Vault sealed')
            else:
                print('Error connecting to Vault')
        except Exception as e:
            print(f'cannot reach Vault: {e}')
        time.sleep(5)


def vault_config(conf, vault_callback=None):
    vault_conf = conf.get('vault', None)
    if vault_conf is None:
        return
    vault_ca = vault_conf.get('ca', None)
    if vault_ca is None:
        verify = True
    else:
        with tempfile.NamedTemporaryFile(
            mode='w', delete=False, encoding='utf-8'
        ) as fd:
            fd.write(vault_ca)
            fd.write('\n')
            verify = fd.name

    vault_url = vault_conf.get('url', None)
    vault_role = vault_conf.get('role', None)
    vault_role_id = vault_conf.get('role_id', None)
    vault_secret = vault_conf.get('secret_id', None)

    if vault_url is None or vault_role_id is None or vault_secret is None:
        raise Exception('Cannot proceed with vault without both token and url')
    wait_for_vault(vault_url, verify)
    req = requests.post(
        f'{vault_url}/v1/auth/approle/login',
        json={'role_id': vault_role_id, 'secret_id': vault_secret},
        verify=verify
    )

    if req.status_code == 200:
        vault_token = req.json().get('auth', {}).get('client_token', None)
    else:
        print(f'Cannot proceed without token ({req.status_code})')
        return

    certs = {}
    vault_certs = vault_conf.get('certificates', [])
    for vault_cert in vault_certs:
        name = vault_cert.get('name', None)
        role = vault_cert.get('role', vault_role)
        ip = vault_cert.get('ip', None)
        alt = vault_cert.get('alt_names', None)
        print(f'Generating certificate {name}')
        json = {'common_name': name}
        if ip is not None:
            json['ip_sans'] = ','.join(ip)
        if alt is not None:
            json['alt_names'] = ','.join(alt)
        req = requests.post(
            f'{vault_url}/v1/pki/issue/{role}',
            headers={'X-Vault-Token': vault_token},
            json=json, verify=verify
        )
        if req.status_code != 200:
            print(
                f'Failed to generate certificate {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
            continue
        data = req.json().get('data', {})
        cert = data.get('certificate', None)
        key = data.get('private_key', None)
        ca_chain = data.get('ca_chain', None)
        if cert is None or key is None or ca_chain is None:
            print(f'Something wrong during generation of cert {name}')
            continue
        certs[name] = (key, cert, ca_chain)

    def vault_transformer(val):
        if isinstance(val, str) and val.startswith('@vault:'):
            vault_entities = val.split(':')
            l_ent = len(vault_entities)
            if l_ent < 2:
                raise Exception('malformed vault reference')
            vault_type = vault_entities[1]
            if vault_type == 'kv1' and l_ent == 4:
                vault_path = vault_entities[2]
                vault_key = vault_entities[3]
                subpath = path.normpath(
                    path.join('secret', vault_role, vault_path))
                req = requests.get(
                    f'{vault_url}/v1/{subpath}',
                    headers={'X-Vault-Token': vault_token}, verify=verify
                )
                if req.status_code == 200:
                    res = req.json().get('data', {}).get(vault_key, None)
                    return res
                else:
                    print(f'Rendering {val}: wrong status {req.status_code}')
            elif vault_type == 'kv2' and l_ent == 4:
                vault_path = vault_entities[2]
                vault_key = vault_entities[3]
                subpath = path.normpath(
                    path.join('kv', vault_role, vault_path))
                req = requests.get(
                    f'{vault_url}/v1/{subpath}',
                    headers={'X-Vault-Token': vault_token}, verify=verify
                )
                if req.status_code == 200:
                    cell = req.json().get('data', {}).get('data', {})
                    res = cell.get(vault_key, None)
                    return res
                else:
                    print(f'Rendering {val}: wrong status {req.status_code}')
            elif vault_type == 'pki-key' and l_ent == 3:
                res = certs.get(vault_entities[2], (None, None, None))[0]
                if res is not None:
                    return res
                else:
                    print(f'could not find translation for {val}')
            elif vault_type == 'pki-cert' and l_ent == 3:
                res = certs.get(vault_entities[2], (None, None, None))[1]
                if res is not None:
                    return res
                else:
                    print(f'could not find translation for {val}')
            elif vault_type == 'pki-ca-chain' and l_ent == 3:
                res = certs.get(vault_entities[2], (None, None, None))[2]
                if res is not None:
                    return res
                else:
                    print(f'could not find translation for {val}')
            elif vault_type == 'pki-chain' and l_ent == 3:
                (key, cert, chain) = certs.get(
                    vault_entities[2], (None, None, None))
                if cert is not None and chain is not None:
                    return cert + '\n' + '\n'.join(chain)
                else:
                    print(f'could not find translation for {val}')
            # Note: pki/ca gives only the intermediate ca. This is not what
            # curl or python requests expect. In ca_chain we are usually only
            # interested in the last certificate (real root)
            elif vault_type == 'ca' and l_ent == 2:
                req = requests.get(
                    f'{vault_url}/v1/pki/ca_chain',
                    verify=verify)
                if req.status_code == 200:
                    return req.content.decode('utf-8')
                else:
                    print('Cannot fetch the CA certificate')
            else:
                print(f'unknown vault request type: {vault_type}')
        return None

    if vault_callback is not None:
        vault_callback(conf, vault_url, vault_token, verify)
    common.transform_json(conf, vault_transformer)


def base_config(sys, conf):
    '''Setup base proxy'''
    os.environ['HOME'] = '/root'
    setup_proxy(sys, conf)
    setup_certificates(conf)


def ntp_config(conf, distro):
    '''Configure ntp client service'''
    ntp_vars = conf.get('ntp')
    chrony_daemon = 'chrony'
    if isinstance(distro, rhel.Distro):
        chrony_daemon = 'chronyd'
    if ntp_vars is not None:
        print('Configure chrony')
        render_template('chrony.tmpl', 'etc/chrony/chrony.conf', ntp_vars)
        subp.subp(['systemctl', 'enable', chrony_daemon])
        subp.subp(['systemctl', 'restart', chrony_daemon])


def get_insecure_registries(conf):
    '''Retrieve the list of insecure registries

    The user provided list may be updated with the nexus itself
    '''
    nexus = conf.get('nexus', None)
    nexus_service = conf.get('nexus_service', None)
    containers_vars = conf.get('containers', {})
    insecure_registries = containers_vars.get('insecure_registries', [])
    if nexus is not None:
        nexus_registry = nexus.get('docker', None)
        if nexus.get('insecure', False) and nexus_registry is not None:
            insecure_registries.append(nexus_registry)
    if nexus_service is not None:
        insecure_registries.append('localhost:8082')
    return insecure_registries


def set_docker_auth(conf):
    '''Set authentication tokens for private registries'''
    containers_vars = conf.get('containers', {})
    raw_auths = containers_vars.get('auths', None)
    k8s_conf = conf.get('kubernetes', None)
    # first destination for docker stand-alone, second for used as kubelet
    # engine
    destination = (
        'root/.docker/config.json' if k8s_conf is None
        else 'var/lib/kubelet/config.json')
    if raw_auths is None:
        return
    auths = [
        {
            "repo": cell.get('repository', ''),
            "token": common.b64(
                cell.get('username', '') + ':' + cell.get('password', '')),
        }
        for cell in raw_auths
    ]
    render_template(
        'dockercfg.tmpl',
        destination,
        {'auths': auths}
    )


def container_engine_docker_config(conf):
    '''Configure the docker container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/docker.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = get_insecure_registries(conf)
    mirrors = conf.get('containers', {}).get('registry_mirrors', None)
    render_template(
        'docker_daemon.tmpl',
        'etc/docker/daemon.json',
        {'insecure_registries': insecure_registries,
         'registry_mirrors': mirrors}
    )
    set_docker_auth(conf)
    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'docker'])
    subp.subp(['systemctl', 'start', 'docker'])


def check_network(spec, iter):
    url = spec.get('http', None)
    if url is not None:
        try:
            print(f'check url {url} - {iter}')
            requests.head(url)
            return True
        except Exception:
            return False
    tcp = spec.get('tcp', None)
    if tcp is not None:
        components = tcp.split(':')
        if len(components) == 2:
            addr = components[0]
            port = int(components[1])
            print(f'check tcp connection {addr}:{port} - {iter}')
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                sock.settimeout(10)
                sock.connect((addr, port))
                return True
            except Exception:
                return False
            finally:
                sock.close()
        else:
            return False
    dns = spec.get('dns', None)
    if dns is not None:
        try:
            print(f'check address {dns} - {iter}')
            socket.getaddrinfo(dns, 0)
            return True
        except Exception:
            return False
    return False


def network_config(conf, distro):
    print('Extract network state')
    state = conf.get('network')
    if state is None:
        print('Network not configured')
        return
    print('Rendering state')
    if 'netplan' in distro.network_conf_fn:
        config = distro.renderer_configs['netplan']
        renderer = netplan.Renderer(config=config)
        renderer.render_network_config(state)
        print('restarting netplan')
        subp.subp(['netplan', 'apply'])
    elif 'sysconfig' in distro.network_conf_fn:
        config = distro.renderer_configs['sysconfig']
        renderer = sysconfig.Renderer(config=config)
        renderer.render_network_config(state)
        # for centos 7 spurious interfaces.
        time.sleep(10)
        for itf in ['eth0', 'eth1']:
            cfg = f'/etc/sysconfig/network-scripts/ifcfg-{itf}'
            if path.exists(cfg):
                os.remove(cfg)
        print('restarting network')
        subp.subp(['systemctl', 'restart', 'network'])
    else:
        print('No supported renderer')
        return
    checks = conf.get('network_checks', [])
    for check in checks:
        iter = 0
        max_tries = check.get('tries', -1)
        while not check_network(check, iter) and iter != max_tries:
            time.sleep(2)
            iter += 1


def configure_admin(conf, distro):
    admin = conf.get('admin', None)
    if admin is not None:
        passwd = admin.get('passwd', None)
        args = {
            'sudo': ['ALL=(ALL) NOPASSWD:ALL'],
            'ssh_authorized_keys': admin.get('keys', []),
            'groups': 'wheel',
            'shell': '/bin/bash',
        }
        if passwd is not None:
            args['plain_text_passwd'] = passwd
            args['lock_passwd'] = False
        distro.create_user('admin', **args)


def configure_base(init, system, conf, vault_callback=None):
    distro = init.distro
    print('Network config')
    network_config(conf, distro)
    print('Base config')
    base_config(system, conf)
    print('Vault config')
    vault_config(conf, vault_callback=vault_callback)
    print('Base admin user config')
    configure_admin(conf, distro)
    print('ntp config')
    ntp_config(conf, distro)


class Unbuffered(object):
    '''A small class to force flushing on  a stream'''

    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


def initialize():
    sys.stdout = Unbuffered(sys.stdout)
    init = stages.Init()
    print('Reading configuration')
    conf = util.read_conf(USER_CONF)
    if path.exists(SYSTEM_CONF):
        system = util.read_conf(SYSTEM_CONF)
    else:
        system = {}
    role = system.get('role', 'core')
    libraries = system.get('libraries') or []
    for library in libraries:
        mod = importlib.import_module(library)
        mod.init()
    return (role, init, conf, system)


def write_status(n):
    target = path.join(ROOT, 'etc/kanod-configure/status')
    with open(target, 'w') as fd:
        fd.write(str(n))


def main():
    print('Starting kanod-runcmd')
    register('core', configure_base)
    (role, init, conf, system) = initialize()
    callback = configuration_table.get(role, None)
    if callback is None:
        print('Unknown role. Exiting without config')
        write_status(1)
        exit(1)
    else:
        try:
            callback(init, system, conf)
            write_status(0)
        except Exception as e:
            write_status(1)
            print(e)


if __name__ == '__main__':
    main()
