#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import base64
import os
from os import path


def setup_certificates(config, cert_folder, suffix=None):
    '''Extract certificates from configuration and write them as files.

    :param config: source configuration (was a yaml file)
    :param cert_folder: target folder for certificates
    '''
    certificates = config.get('certificates', {})
    if not path.exists(cert_folder):
        os.mkdir(cert_folder)
    for (cert_name, cert_value) in certificates.items():
        full_name = cert_name if suffix is None else cert_name + suffix
        with open(path.join(cert_folder, full_name), 'w') as fd:
            fd.write(cert_value)
            fd.write('\n')
    vault_ca = config.get('vault', {}).get('ca', None)
    if vault_ca is not None:
        full_name = 'vault' if suffix is None else 'vault' + suffix
        with open(path.join(cert_folder, full_name), 'w') as fd:
            fd.write(vault_ca)
            fd.write('\n')


def transform_json(json, filter_transform):
    '''Apply a filter on all the leaves of a json-like structure

    The function operates as side effect and transforms the structure.
    A single isolated leaf cannot be transformed.

    :param json: structure to transform (dictionnary/list tree of leaves)
    :filter_transform: function to apply on leaves.
    '''
    if isinstance(json, list):
        for (i, v) in enumerate(json):
            r = filter_transform(v)
            if r is not None:
                json[i] = r
            else:
                transform_json(v, filter_transform)
    elif isinstance(json, dict):
        for (k, v) in json.items():
            r = filter_transform(v)
            if r is not None:
                json[k] = r
            else:
                transform_json(v, filter_transform)
    else:
        pass


def b64(s):
    '''encode a utf-8 string in base64

    :param s: string to encode
    :return: encoded ascii string
    '''
    return base64.b64encode(s.encode('utf-8')).decode('ascii')
