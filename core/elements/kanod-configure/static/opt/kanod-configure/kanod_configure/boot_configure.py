#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import importlib
from os import path
import subprocess
import sys

from cloudinit import util
from typing import Any, Callable, Dict

SYSTEM_CONF = '/etc/kanod-configure/system.yaml'

boot_configuration_table: Dict[str, Callable[[Any, Any], None]] = {}


def register_boot(name: str, callback: Callable[[Any, Any], None]):
    boot_configuration_table[name] = callback


class Unbuffered(object):
    '''A small class to force flushing on  a stream'''

    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


def initialize():
    print('initialize boot configure')
    if path.exists(SYSTEM_CONF):
        system = util.read_conf(SYSTEM_CONF)
    else:
        system = {}
    role = system.get('role', 'default')
    print(f'found role {role}')
    libraries = system.get('libraries') or []
    for library in libraries:
        mod = importlib.import_module(library)
        if hasattr(mod, 'init_boot'):
            mod.init_boot()
    return (role, system)


# TODO(pc): It is not clear that we want to keep it that way.
# A better abstraction would be a description of the huge pages we want
# for example.
def configure_grub(conf):
    '''Configure grub command line at first boot'''
    print('grub configure')
    grub_line = conf.get('grub', None)
    if grub_line:
        command = ['/usr/local/bin/grub-init', grub_line]
        proc = subprocess.run(
            command, stdout=sys.stdout, stderr=subprocess.STDOUT)
        if proc.returncode != 0:
            print('grubinit failed')


def default_callback(system, conf):
    configure_grub(conf)


def main():
    # read_conf calls open. open can be given a file descriptor and will
    # wrap it. So we are just reading the config from stdin
    print('starting kanod-bootcmd')
    conf = util.read_conf(sys.stdin.fileno())
    (role, system) = initialize()
    callback = boot_configuration_table.get(role, default_callback)
    try:
        callback(system, conf)
        print('end of kanod bootcmd')
        exit(0)
    except Exception as e:
        print('error during kanod bootcmd')
        print(e)
        exit(1)


if __name__ == '__main__':
    main()
