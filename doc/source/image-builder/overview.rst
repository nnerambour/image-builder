Usage
=====

This is the core tool for building OS images using kanod-configure in cloud-init
for specialization. Images are built using information in specific folders given
on the command line.

A typical call is:

    kanod-image-builder -s 'key=value' core <additional folders>

``kanod-node``, ``common-services``, ``gogs-service`` are examples of projects
providing specialized images for Kanod.
